﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{

    public class BaseDAO<T> : IDAO<T>
    {
        // Instants of Database
        protected Database database = new Database();

        // name table
        protected virtual string TableName { get; set; }

        /// <summary>
        /// Insert object of list object
        /// </summary>
        /// <param name="row"></param>
        /// <returns>1 - Insert success, 0 - Insert error</returns>
        public int insert(T row)
        {

            int result = database.InsertTable($"{TableName}", row);
            return result;

        }

        /// <summary>
        /// Update object of list object
        /// </summary>
        /// <param name="row"></param>
        /// <returns>1 - Update success, 0 - Update error</returns>
        public int update(T row)
        {
            int result = database.UpdateTable($"{TableName}", row);
            return result;
        }


        /// <summary>
        /// Delete object of list object
        /// </summary>
        /// <param name="row"></param>
        /// <returns>true - Delete success, false - Delete unsuccess</returns>
        public bool delete(T row)
        {
            var check = false;
            int result = database.DeleteTable($"{TableName}", row);
            if (result == 1)
            {
                return check = true;
            }
            else return check;
        }

        // Get all object of list object
        public List<object> findAll()
        {
            List<object> objects = new List<object>();
            objects = database.FindAllTable($"{TableName}");
            return objects;
        }

        /// <summary>
        /// Get all object of list object by name
        /// </summary>
        /// <param name="name"></param>
        public object findByName(string name)
        {
            object result = new Object();
            result = database.SelectTable($"{TableName}", name);
            return result;
        }

        /// <summary>
        /// Delete all object of list 
        /// </summary>
        /// <param name="ts"></param>
        /// <returns>1 - Delete success, 0 - Delete unsuccess</returns>
        public int truncate()
        {
            int result = database.TruncateTable($"{TableName}");
            return result;
        }
    }
}
