﻿using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{
    // Information of Database
    public class Database
    {
        // List of Product
        protected List<Product> productTable;

        // List of Category
        protected List<Category> categoryTable;

        // List of Accessory
        protected List<Accessory> accessoryTable;

        // get, set of list Category
        public List<Product> ProductTable { get { return this.productTable; } set { this.productTable = value; } }

        // get, set of list Category
        public List<Category> CategoryTable { get { return this.categoryTable; } set { this.categoryTable = value; } }

        // get, set of list Accessory
        public List<Accessory> AccessoryTable { get { return this.accessoryTable; } set { this.accessoryTable = value; } }



        // Set name of productTable
        const string  PRODUCT_TABLE_NAME = "productTable";

        // Set name of categoryTable
        const string ListCategory = "categoryTable";

        // Set name of accessoryTable
        const string ListcAcessory = "accessoryTable";

        // Contructor no parameter
        public Database()
        {

        }

        // singleton 
        protected static Database instants;

        /// <returns>return instants</returns>
        public static Database getInstants()
        {

            if (instants == null)
            {
                return instants = new Database();
            }
            return instants;
        }

        /// <summary>
        /// Insert object of list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="row"></param>
        /// <returns>1 - insert success, 0 - insert error</returns>
        public int InsertTable(string name, object row)
        {
            if (name == ListProduct)
            {
                productTable.Add((Product)row);
                return 1;
            }
            if (name == ListCategory) ;
            {
                categoryTable.Add(row as Category);
                return 1;
            }
            if (name == ListcAcessory)
            {
                accessoryTable.Add((Accessory)row);
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Get all object of List
        /// </summary>
        /// <param name="name"></param>
        /// <param name="where"></param>
        public object SelectTable(string name, string where)
        {
            if (name == ListProduct)
            {
                foreach (Product product in productTable)
                {
                    if (product.Name == where)
                    {
                        var result = product;
                        return result;
                    }
                }
            }
            if (name == ListCategory)
            {
                foreach (Category category in categoryTable)
                {
                    if (category.Name == where)
                    {
                        var result = category;
                        return result;
                    }
                }
            }
            if (name == ListcAcessory)
            {
                foreach (Accessory accessotion in accessoryTable)
                {
                    if (accessotion.Name == where)
                    {
                        var result = accessotion;
                        return result;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Update object of list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="row"></param>
        /// <returns>1 - update success, 0 - update error</returns>
        public int UpdateTable(string name, object row)
        {

            if (name == ListProduct)
            {
                foreach (Product product in productTable)
                {
                    var productUpdate = (Product)row;
                    if (product.Id == productUpdate.Id)
                    {
                        product.Name = productUpdate.Name;
                        product.CategoryId = productUpdate.CategoryId;
                        return 1;
                    }
                }
            }
            if (name == ListCategory)
            {
                foreach (Category category in categoryTable)
                {
                    var categoryUpdate = (Category)row;
                    if (category.Id == categoryUpdate.Id)
                    {
                        category.Name = categoryUpdate.Name;
                        return 1;
                    }
                }
            }
            if (name == ListcAcessory)
            {
                foreach (Accessory accessotion in accessoryTable)
                {
                    var accessotionUpdate = (Accessory)row;
                    if (accessotion.Id == accessotionUpdate.Id)
                    {
                        accessotion.Name = accessotionUpdate.Name;
                        return 1;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Delete object of list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="row"></param>
        /// <returns>1 - Delete success, 0 - Delete usuccess</returns>
        public int DeleteTable(string name, object row)
        {
            if (name == ListProduct)
            {
                var deleteProduct = (Product)row;
                foreach (Product product in productTable)
                {
                    if (product.Id == deleteProduct.Id)
                    {
                        productTable.Remove(product);
                        return 1;
                    }

                }
            }
            if (name == ListCategory)
            {
                var deleteCategory = (Category)row;
                foreach (Category category in categoryTable)
                {
                    if (category.Id == deleteCategory.Id)
                    {
                        categoryTable.Remove(category);
                        return 1;

                    }
                }
            }
            if (name == ListcAcessory)
            {
                var deleteAccessory = (Accessory)row;
                foreach (Accessory accessotion in accessoryTable)
                {
                    if (accessotion.Id == deleteAccessory.Id)
                    {
                        accessoryTable.Remove(accessotion);
                        return 1;
                    }

                }
            }
            return 0;
        }

        /// <summary>
        /// Delete all object of list
        /// </summary>
        /// <param name="name"></param>
        /// <returns>1 - Delete success, 0 - Delete error</returns>
        public int TruncateTable(string name)
        {
            if (name == ListProduct)
            {
                productTable.Clear();
                return 1;
            }
            if (name == ListCategory)
            {
                categoryTable.Clear();
                return 1;
            }
            if (name == ListcAcessory)
            {
                accessoryTable.Clear();
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Update table by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="row"></param>
        /// <returns>1 - Update success, 0 - Update error</returns>
        public int UpdateTableById(int id, object row)
        {
            Product productById = new Product();
            Category categoryById = new Category();
            Accessory accessotionById = new Accessory();
            if (productById == row)
            {
                foreach (Product product in productTable)
                {
                    if (product.Id == id)
                    {
                        product.Name = productById.Name;
                        product.CategoryId = productById.CategoryId;
                        return 1;
                    }
                }
            }
            if (categoryById == row)
            {
                foreach (Category category in categoryTable)
                {
                    category.Name = categoryById.Name;
                    return 1;
                }
            }
            if (accessoryTable == row)
            {
                foreach (Accessory accessotion in accessoryTable)
                {
                    accessotion.Name = productById.Name;
                    return 1;
                }
            }
            return 0;
        }

        /// <summary>
        /// Get all object of List
        /// </summary>
        /// <param name="name"></param>
        public List<object> FindAllTable(string name)
        {
            List<object> objects = new List<object>();
            if (name == ListProduct)
            {
                foreach (Product product in productTable)
                {
                    objects.Add(product);
                    return objects;
                }
            }
            if (name == " categoryTable")
            {
                foreach (Category category in categoryTable)
                {
                    objects.Add(category);
                    return objects;
                }
            }
            if (name == ListcAcessory)
            {
                foreach (Accessory accessotion in accessoryTable)
                {
                    accessoryTable.Add(accessotion);
                    return objects;
                }
            }
            return objects;
        }
    }
}

