﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{
    //interface of IDAO
    public interface IDAO<T>
    {
        //insert obj of list
        // <returns>1 - Insert success, 0 - Insert error</returns>
        int insert(T t);

        //update obj of list
        // <returns>1 - update success, 0 - update error</returns>
        int update(T t);

        //delete obj of list
        /// <returns>true - Delete success,false - Delete error</returns>
        bool delete(T t);

        //get all obj of list
        /// <returns>list of obj</returns>
        List<object> findAll();

        //get obj of list obj by name
        /// <returns>Information of obj</returns>
        object findByName(string name);

        //delete all obj of list
        /// <returns>1 - Delete success, 0 - Delete error</returns>
        int truncate();
    }
}
