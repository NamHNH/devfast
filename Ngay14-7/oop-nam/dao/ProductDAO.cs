﻿using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{

    public class ProductDAO : BaseDAO<Product>
    {
        // name of table
        private string TableName { get { return TableName; } set { TableName = "product"; } }

        /// <summary>
        /// Get all Product in list Product by name
        /// </summary>
        /// <param name="where"></param>
        /// <returns>List of Product</returns>
        public List<Product> Search(string where)
        {
            List<Product> products = new List<Product>();
            foreach (Product product in database.ProductTable)
            {
                if (product.Id == Int16.Parse(where) || product.Name == where || product.CategoryId == Int16.Parse(where))
                {
                    products.Add(product);
                }
            }
            return products;

        }
    }

}
