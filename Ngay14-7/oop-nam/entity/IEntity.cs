﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.entity
{
    public interface IEntity
    {
        // id of object
        int Id { get; set; }

        // name of object
        string Name { get; set; }
    }
}
