﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.entity
{
    // Information of Product
    public class Product : BaseRow
    {
        // Primary key of table Category
        public int CategoryId { get; set; }

        // Contructor no parameter
        public Product() : base()
        {
        }
        /// <summary>
        /// Contructor parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="categoryId"></param>
        public Product(int id, string name, int categoryId) : base(id, name)
        {
        }
    }
}
