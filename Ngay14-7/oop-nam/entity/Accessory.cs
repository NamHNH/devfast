﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.entity
{
    // Information of Accessory
    public class Accessory : BaseRow
    {
        // Contructor no parameter
        public Accessory()
        {
        }
        /// <summary>
        /// Contructor parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public Accessory(int id, string name) : base(id, name)
        {
        }
    }
}
