﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.entity
{
    // Information of BaseRow
    public class BaseRow : IEntity
    {
        // Primary key of Table T
        public int Id { get; set; }

        // Name of T
        public string Name { get; set; }

        // Contructor no parameter
        public BaseRow()
        {
        }
        /// <summary>
        /// Contructor parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public BaseRow(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
