﻿using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.demo
{
    public class ProductDemo
    {
        // Create Product
        public Product CreateProductTest(int id, string name, int categoryId)
        {
            Product product = new Product(id, name, categoryId);

            /* product.id = id;
            product.name = name;
            product.categoryId = categoryId;*/
            return product;

        }

        // Print information of Product
        public void PrintProduct(List<Product> products)
        {
            for (int i = 0; i < products.Count; i++)
            {
                Console.WriteLine("id product la : " + products[i].Id);
                Console.WriteLine("name product la : " + products[i].Name);
                Console.WriteLine("categoryId product la : " + products[i].CategoryId);
            }

        }
    }
}
