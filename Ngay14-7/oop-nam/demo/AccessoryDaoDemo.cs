using OOP.dao;
using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.demo
{
    public class AccessoryDaoDemo : AccessoryDAO
    {

        /// Insert Accessory in list Accessory
        public void InsertTest()
        {
            Accessory accessory = new Accessory();
            accessory.Id = database.AccessoryTable.Count + 1;
            Console.WriteLine("name of accessory: ");
            accessory.Name = Console.ReadLine();
            int result = insert(accessory);
            if (result == 1)
            {
                Console.WriteLine("insert accessory success!");
            }
            else
            {
                Console.WriteLine("insert accessory error!");
            }
        }

        /// Get all Accessory of list Accessory
        public void FindAllTest()
        {
            List<object> objects = new List<object>();
            objects = findAll();
            foreach (Accessory accessory in objects)
            {
                Console.WriteLine(accessory.Id + " " + accessory.Name);
            }
        }

        /// Update Accessory in list Accessory
        public void UpdateTest()
        {
            Accessory accessory = new Accessory();
            Console.WriteLine("id of accessory: ");
            accessory.Id = Int16.Parse(Console.ReadLine());
            Console.WriteLine("name of accessory: ");
            accessory.Name = Console.ReadLine();
            int result = update(accessory);
            if (result == 1)
            {
                Console.WriteLine("Update Accessory success!");
            }
            else
            {
                Console.WriteLine("Update Accessory error!");
            }

        }
    }
}
