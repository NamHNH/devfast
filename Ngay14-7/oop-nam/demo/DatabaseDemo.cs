﻿using OOP.dao;
using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.demo
{
    public class DatabaseDemo : Database
    {
        // Insert object of list
        public void InsertTableTest()
        {
            Product product = new Product();
            product.Id = ProductTable.Count + 1;
            product.Name = "nam";
            product.CategoryId = 123;
            product.Id = Int16.Parse(Console.ReadLine());

            int result = InsertTable("productTable", product);
            if (result == 1)
            {
                Console.WriteLine("Insert Product success");
            }
            else
            {
                Console.WriteLine("Insert Product error");
            }
            Console.WriteLine("hello");
            Console.ReadLine();
        }

        // Get all object of list
        public void selectTableTest()
        {
            SelectTable("productTable", "name_1");
        }

        // Update object of list
        public void UpdateTableTest()
        {
            Product product = new Product();
            Console.Write("id of Product: ");
            product.Id = Int16.Parse(Console.ReadLine());

            int result = UpdateTable("productTable", product);
            if (result == 1)
            {
                Console.WriteLine("Update Product success");
            }
            else
            {
                Console.WriteLine("Update Product error!");
            }
            Console.ReadLine();
        }

        // Delete object of list
        public void DeleteTableTest()
        {
            Product product = new Product();
            Console.Write("Id of product: ");
            product.Id = Int16.Parse(Console.ReadLine());
            int result = DeleteTable("productTable", product);
            if (result == 1)
            {
                Console.WriteLine("delete product success!");
            }
            else
            {
                Console.WriteLine("delete product error!");
            }
        }

        // Delete all object of list
        public void TruncateTableTest()
        {
            int result = TruncateTable("productTable");
            if (result == 1)
            {
                Console.WriteLine("delete productTable success!");
            }
            else
            {
                Console.WriteLine("delete product error!");
            }
        }

        // init database list
        public void InitDatabase()
        {

            // Data of productTable
            productTable = new List<Product> {
                 new Product(1, "h", 1) ,
                 new Product(2, "o", 3) ,
                 new Product(3, "a", 0) ,
                 new Product(4, "n", 7) ,
                 new Product(5, "g", 1) ,
                 new Product(6, "n", 9) ,
                 new Product(7, "a", 9) ,
                 new Product(8, "m", 9) ,
                 new Product(9, "a", 0) ,
                 new Product(10, "b", 2) ,
            };
            // Data of categoryTable
            categoryTable = new List<Category>
            {
                 new Category(1, "a"),
                 new Category(2, "ab"),
                 new Category(3, "abc"),
                 new Category(4, "abcd"),
                 new Category(5, "abcde"),
                 new Category(6, "abcdef"),
                 new Category(7, "abcdefi"),
                 new Category(8, "abcdefij"),
                 new Category(9, "abcdefiijk"),
                 new Category(10, "abcdefijkh"),
            };
            // Data of accessoryTable
            accessoryTable = new List<Accessory>
            {
                new Accessory(1, "name1"),
                new Accessory(2, "name2"),
                new Accessory(3, "name3"),
                new Accessory(4, "name4"),
                new Accessory(5, "nam5"),
                new Accessory(6, "name6"),
                new Accessory(7, "name7"),
                new Accessory(8, "name8"),
                new Accessory(9, "name9"),
                new Accessory(10, "name10"),

            };



        }

        /// <summary>
        /// Print all object of list
        /// </summary>
        /// <param name="name"></param>
        public void PrintTableTest(string name)
        {
            if (name == "productTable")
            {
                foreach (Product product in productTable)
                {
                    Console.WriteLine(product.Name + " " + product.Name + " " + product.CategoryId);
                }
            }
            if (name == "categoryTable")
            {
                foreach (Category category in categoryTable)
                {
                    Console.WriteLine(category.Id + " " + category.Name);
                }
            }
            if (name == "accessoryTable")
            {
                foreach (Accessory accessotion in accessoryTable)
                {
                    Console.WriteLine(accessotion.Id + " " + accessotion.Name);
                }
            }
        }

        /// Update object of list table 
        public void UpdateTableById()
        {
            Product product = new Product();
            Console.Write("Id of product: ");
            int id = Int16.Parse(Console.ReadLine());
            Console.Write("name of product: ");
            product.Name = Console.ReadLine();
            int result = UpdateTableById(id, product);
            if (result == 1)
            {
                Console.WriteLine("update by id product success!");
            }
            else
            {
                Console.WriteLine("update by id product error!");
            }
        }
    }

}
