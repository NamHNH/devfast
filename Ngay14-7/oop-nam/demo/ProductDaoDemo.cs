﻿using OOP.dao;
using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.demo
{
    public class ProductDaoDemo : ProductDAO
    {

        // Insert Product in list Product
        public void InsertTest()
        {
            Product product = new Product();
            product.Id = database.ProductTable.Count + 1;

            Console.WriteLine("Name of Product: ");
            product.Name = Console.ReadLine();

            int Result = insert(product);
            if (Result == 1)
            {
                Console.WriteLine("Insert Product success!");
            }
            else
            {
                Console.WriteLine("Insert Product error!");
            }
        }

        // Get all Product of list Product
        public void FindAllTest()
        {
            List<object> objects = new List<object>();
            objects = findAll();
            foreach (Product product in objects)
            {
                Console.WriteLine(product.Id + "     " + product.Name + "     " + product.CategoryId);
            }
        }

        // Update Product in list Product
        public void UpdateTest()
        {
            Product product = new Product();

            Console.WriteLine("Id of Product: ");
            product.Id = Int16.Parse(Console.ReadLine());

            Console.WriteLine("Name of Product: ");
            product.Name = Console.ReadLine();

            Console.WriteLine("Id of Category: ");
            product.CategoryId = Int16.Parse(Console.ReadLine());

            int result = update(product);
            if (result == 1)
            {
                Console.WriteLine("Update product success!");
            }
            else
            {
                Console.WriteLine("Update product error!");
            }
        }

        // delete Product in list Product
        public void DeleteTest()
        {
            Product product = new Product();
            product.Id = Int16.Parse(Console.ReadLine());
            bool result = delete(product);
            if (result == true)
            {
                Console.WriteLine("delete product success!");
            }
            else
            {
                Console.WriteLine("delete product error!");
            }
        }

        // delete all list Product
        public void TruncateTest()
        {
            Product product = new Product();
            int result = truncate();
            if (result == 1)

            {
                Console.WriteLine("truncate product success!");
            }
            else
            {
                Console.WriteLine("truncate product error!");
            }

        }
    }
}
