<?php
function sortByPrice($listProduct)
{
    $countProduct = count($listProduct);
    for ($i = 0; $i< ($countProduct - 1); $i++)
    {
        for ($j = $i + 1; $j < $countProduct; $j++)
        {
            if ($listProduct[$i]['price'] > $listProduct[$j]['price'])
            {
                $tmp = $listProduct[$i];
                $listProduct[$i] = $listProduct[$j];
                $listProduct[$j]= $tmp;
            }
        }
    }
    return $listProduct;
}
function printscreen($listProduct) {
    for ($i= 0;$i<count($listProduct);$i++){
        print_r($listProduct[$i]);
    }
}
$listProduct = [
    ['name'=>'CPU', 'price'=>750, 'quality'=>10, 'categroyID'=>1],
    ['name'=>'RAM', 'price'=>50, 'quality'=>2, 'categroyID'=>2],
    ['name'=>'HDD', 'price'=>70, 'quality'=>1, 'categroyID'=>2],
    ['name'=>'Main', 'price'=>400, 'quality'=>3, 'categroyID'=>1],
    ['name'=>'Keyboard', 'price'=>30, 'quality'=>8, 'categroyID'=>1],
    ['name'=>'Mouse', 'price'=>25, 'quality'=>50, 'categroyID'=>4],
    ['name'=>'VGA', 'price'=>60, 'quality'=>35, 'categroyID'=>4],
    ['name'=>'Monitor', 'price'=>120, 'quality'=>28, 'categroyID'=>2],
    ['name'=>'Case', 'price'=>120, 'quality'=>28, 'categroyID'=>5]
];
$listProduct = sortByPrice($listProduct);
echo '<pre>';
printscreen($listProduct);
?>
