
export const BASE_URL = 'http://localhost:3000/blogs'
export const LIST_CATEGORY = [
        {categoryId: 0, name: 'Kinh doanh'},
        {categoryId: 1, name: 'Giải trí'},
        {categoryId: 2, name: 'Thế giới'},
        {categoryId: 3, name: 'Thời sự'},
        {categoryId: 4, name: 'Văn hóa'},
        {categoryId: 5, name: 'Nghệ thuật'},
        {categoryId: 6, name: 'Chính trị'},
        {categoryId: 7, name: 'Lịch sử'},
        {categoryId: 8, name: 'Thể thao'},
        {categoryId: 9, name: 'Y tế'},
        {categoryId: 10, name: 'Giao thông'},
        {categoryId: 11, name: 'Hội họa'},
        {categoryId: 12, name: 'ShowBizz'},
]

export const LIST_POSITION = [
    {positionId: 1 , positionName: 'Việt Nam'},
    {positionId: 2 , positionName: 'Châu Á'},
    {positionId: 3 , positionName: 'Châu Âu'},
    {positionId: 4 , positionName: 'Châu Mỹ'},
]
