var app = new Vue({
    el : "#app",
    data : {
        message : "Hello World!",
        checked : true,
        checkedNames : [],
        picked : [],
        selected : '',
        categorySelected : [],
        categoryList: [
            { id: 0, name: 'Iphone' },
            { id: 1, name: 'SamSung' },
            { id: 2, name: 'OPPO' },
            { id: 3, name: 'HTC' }
        ],
        categorySelect : '',
        year : 0
    }
});